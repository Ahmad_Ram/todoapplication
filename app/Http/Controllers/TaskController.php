<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        return response()->json(["Data"=>Task::all()],200);
    }

    public function show($id)
    {
        $task = Task::find($id);
        if($task){
            return response()->json(["Data"=>$task],200);
        }
        return response()->json(["message"=>"Task not found"],404);
    }

    public function create(Request $request)
    {
        $name = $request->input('name');

        Task::create([
            'name'=>$name,
            'status_id'=>1,
        ]);

        return response()->json(["message"=>"Task was created successfully!!"],200);
    }

    public function delete($id)
    {

        $task = Task::find($id);

        if($task){
            $task->delete();
            return response()->json(["message"=>"Task was deleted successfully!!"],200);
        }
        return response()->json(["message"=>"Task not found"],404);
    }

    public function edit(Request $request, $id)
    {

        $task = Task::find($id);

        if(!$task){
            $task->delete();
            return response()->json(["message"=>"Task was not found!!"],404);
        }

        $status = Status::find($request->input('status_id'));

        if(!$status){
            return response()->json(["message"=>"Status was not found!!"],404);
        }

        $task->name=$request->input('name');
        $task->status_id=$request->input('status_id');

        if($task->save()){
            return response()->json(["message"=>"Task was edited successfully!!","Data"=>$task],200);
        }
        return response()->json(["message"=>"General Error"],500);

    }
}
