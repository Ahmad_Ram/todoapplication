<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name', 'status_id',
    ];

    public function status(){
        return $this->belongsTo('app\Models\Status','status_id');
    }
}
