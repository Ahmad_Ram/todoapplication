<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
            'name'=>'Not yet'
        ]);

        Status::create([
            'name'=>'Done'
        ]);

    }
}
