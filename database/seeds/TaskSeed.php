<?php

use Illuminate\Database\Seeder;
use App\Models\Task;

class TaskSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::create([
            'name'=>'Cleaning the room',
            'status_id'=>1
        ]);
        Task::create([
            'name'=>'Picking up your mom',
            'status_id'=>2
        ]);
        Task::create([
            'name'=>'Check on your grandmother',
            'status_id'=>1
        ]);
    }
}
