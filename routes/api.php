<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('tasks','TaskController@index');
Route::get('tasks/{id}','TaskController@show');
Route::post('tasks','TaskController@create');
Route::delete('tasks/{id}','TaskController@delete');
Route::patch('tasks/{id}','TaskController@edit');
